#include <stddef.h>
#include "ll_cycle.h"

int ll_has_cycle(node *head) {
    /* TODO: Implement ll_has_cycle */
    node *slow_node = NULL;
    node *fast_node = NULL;

    if (head == NULL) {
        return 0;
    }

    slow_node = head;
    fast_node = head->next;

    while (slow_node != NULL) {
        if (slow_node == fast_node) {
            return 1;
        } else {
            slow_node = slow_node->next;

            if (fast_node != NULL && fast_node->next != NULL) {
                fast_node = fast_node->next->next;
            } else {
                break;
            }
        }
    }

    return 0;
}
